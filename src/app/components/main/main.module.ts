import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { SharedModule } from '../../shared/shared.module';

import { MainComponent } from './main.component';
import { ContentPageComponent } from './content-page/content-page.component';
import { ContentSidebarComponent } from './content-sidebar/content-sidebar.component';

@NgModule({
  imports: [CommonModule, MainRoutingModule, SharedModule],
  declarations: [MainComponent, ContentSidebarComponent, ContentPageComponent]
})
export class MainModule {}
