import { Component, OnInit } from '@angular/core';
declare let d3: any;

import { Data } from './bullet-data';

@Component({
  selector: 'app-d3-bullet',
  templateUrl: './d3-bullet.component.html',
  styleUrls: ['./d3-bullet.component.scss']
})
export class D3BulletComponent implements OnInit {
  options;
  data;
  ngOnInit() {
    this.options = {
      chart: {
        type: 'bulletChart',
        height: 20,
        margin : {
          top: 0,
          right: 20,
          bottom: 0,
          left: 125
        }
      }
    };
    this.data = Data;
    d3.selectAll('nvd3').style('fill', '#ffffff');
  }
}
